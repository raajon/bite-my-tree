import {
  Color3,
  Color4,
  PBRMaterial,
  Scene,
  StandardMaterial,
  Texture
} from "@babylonjs/core";

export default class Materials{

  private static _grassMaterial: StandardMaterial;
  private static _redBombMaterial: StandardMaterial;
  private static _blueBombMaterial: StandardMaterial;
  private static _pinkBombMaterial: StandardMaterial;




  public static getGrassMaterial(scene: Scene): StandardMaterial{
      if(!this._grassMaterial){
        this._grassMaterial = new StandardMaterial("grass", scene);
        this._grassMaterial.diffuseColor = new Color3(.1,.7,0);
      }
      return this._grassMaterial;
  }

  public static getRedBombMaterial(scene: Scene): StandardMaterial{
      if(!this._redBombMaterial){
        this._redBombMaterial = new StandardMaterial("redBomb", scene);
        this._redBombMaterial.diffuseColor = new Color3(1,0,0);
      }
      return this._redBombMaterial;
  }

  public static getBlueBombMaterial(scene: Scene): StandardMaterial{
      if(!this._blueBombMaterial){
        this._blueBombMaterial = new StandardMaterial("blueBomb", scene);
        this._blueBombMaterial.diffuseColor = new Color3(0,0,1);
      }
      return this._blueBombMaterial;
  }

  public static getPinkBombMaterial(scene: Scene): StandardMaterial{
      if(!this._pinkBombMaterial){
        this._pinkBombMaterial = new StandardMaterial("pinkBomb", scene);
        this._pinkBombMaterial.diffuseColor = new Color3(1,0,.5);
      }
      return this._pinkBombMaterial;
  }

}
