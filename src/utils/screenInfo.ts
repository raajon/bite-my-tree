export default class ScreenInfo{
    private static instance: ScreenInfo;

    private _isMobile:boolean = false;
    private _isPortrait:boolean = false;
    private _width:number;
    private _height:number;

    constructor(){
      if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
          this._isMobile = true;
      }
      if (window.matchMedia("(orientation: portrait)").matches) {
        this._isPortrait = true;
      }
      this._width = window.screen.width;
      this._height = window.screen.height;

      console.log(this);
    }

    public static getInstance():ScreenInfo {
        if (!ScreenInfo.instance) {
            ScreenInfo.instance = new ScreenInfo();
        }
        return ScreenInfo.instance;
    }

    public isMobile(){
      return this._isMobile;
    }

    public isPortrait(){
      return this._isPortrait;
    }

    public getWidth():number{
      return this._width;
    }

    public getHeight():number{
      return this._height;
    }
}
