import { ActionManager, ExecuteCodeAction, Scalar, Scene } from '@babylonjs/core';
import Hud from './ui/Hud';

export class InputController {

    public inputMap: any;
    private _scene: Scene;

    //simple movement
    public horizontal: number = 0;
    public vertical: number = 0;
    //tracks whether or not there is movement in that axis
    public horizontalAxis: number = 0;
    public verticalAxis: number = 0;

    public jumpKeyDown: boolean = false;

    //Mobile Input trackers
    private _hud: Hud;
    private _mobileJump: boolean;
    private _mobileDash: boolean;

    //Camera height controls
    private pcCameraDown: boolean = false;
    private pcCameraUp: boolean = false;

    public readonly _treeSelectCount = 7;

    private btnHeatmap: boolean = false;

    constructor(scene: Scene, hud: Hud) {

        this._scene = scene;
        this._hud = hud;

        //scene action manager to detect inputs
        this._scene.actionManager = new ActionManager(this._scene);

        this.inputMap = {};
        this._scene.actionManager.registerAction(new ExecuteCodeAction(ActionManager.OnKeyDownTrigger, (evt) => {
            this.inputMap[evt.sourceEvent.key] = evt.sourceEvent.type == "keydown";
        }));
        this._scene.actionManager.registerAction(new ExecuteCodeAction(ActionManager.OnKeyUpTrigger, (evt) => {
          if(evt.sourceEvent.key !== "h" || evt.sourceEvent.key !== "H"){
            this.inputMap[evt.sourceEvent.key] = evt.sourceEvent.type == "keydown";
          }
        }));

        scene.onBeforeRenderObservable.add(() => {
            this._updateFromKeyboard();
        });
    }

    private _updateFromKeyboard(): void {

        if ((this.inputMap["w"] ||this.inputMap["ArrowUp"] )) {
            this.verticalAxis = 1;
            this.vertical = Scalar.Lerp(this.vertical, 1, 0.2);

        } else if ((this.inputMap["s"] ||this.inputMap["ArrowDown"])) {
            this.vertical = Scalar.Lerp(this.vertical, -1, 0.2);
            this.verticalAxis = -1;
        } else {
            this.vertical = 0;
            this.verticalAxis = 0;
        }

        if ((this.inputMap["a"] ||this.inputMap["ArrowLeft"])) {
            this.horizontal = Scalar.Lerp(this.horizontal, -1, 0.2);
            this.horizontalAxis = -1;

        } else if ((this.inputMap["d"] ||this.inputMap["ArrowRight"])) {
            this.horizontal = Scalar.Lerp(this.horizontal, 1, 0.2);
            this.horizontalAxis = 1;
        }
        else {
            this.horizontal = 0;
            this.horizontalAxis = 0;
        }

        (this.inputMap["q"] || this.inputMap["Q"] || this._mobileJump)  ? this.pcCameraDown = true : this.pcCameraDown = false;
        (this.inputMap["e"] || this.inputMap["E"] || this._mobileDash)  ? this.pcCameraUp = true : this.pcCameraUp = false;


        //Jump Checks (SPACE)
        if ((this.inputMap[" "] || this._mobileJump)) {
            this.jumpKeyDown = true;
        } else {
            this.jumpKeyDown = false;
        }
    }

    public getPcCameraUp(){
      return this.pcCameraUp;
    }

    public getPcCameraDown(){
      return this.pcCameraDown;
    }

}
