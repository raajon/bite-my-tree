import {
  Color3,
  DirectionalLight,
  GlowLayer,
  HemisphericLight,
  Mesh,
  PointLight,
  RenderTargetTexture,
  Scene,
  ShadowGenerator,
  Texture,
  Vector3
} from "@babylonjs/core";
import Map from './map';

// import Map from './map_simple.ts';

export class Environment {
    private _scene: Scene;
    private _map: Map;

    private _lightH:HemisphericLight

    constructor(scene: Scene) {
        this._scene = scene;
    }

    public async load() {

      const  gl = new GlowLayer("glow", this._scene);

      this._lightH = new HemisphericLight("lightH", new Vector3(-1, 1, 1), this._scene);
      this._lightH.specular = new Color3(0, 0, 0);
      // this._lightH.specularPower = 0;
      this._lightH.diffuse = new Color3(1, 1, 1);
      // this._lightH.intensity = 0;
      this._lightH.intensity = 0.51;

      const map:Map = new Map(this._scene);
      await map.load();

      // this._scene.fogMode = Scene.FOGMODE_EXP;
      // this._scene.fogColor = new Color3(0.6, 0.6, 0.6);
      // this._scene.fogDensity = 0.01;

    }
}
