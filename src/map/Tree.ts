import {
  Material,
  Mesh,
  Scene,
  TransformNode,
  Vector3,
} from "@babylonjs/core";
import Materials from "../Materials";
import Models from "../Models";

export default class Tree {
  private _scene: Scene;
  private _mesh: Mesh;
  private _slots:TransformNode[];
  private _slotsIterator:number = 0;
  private _decorations:Mesh[];
  private _devorationIterator:number = 0;

  constructor(position:Vector3, scene: Scene) {
    this._mesh = Models.getInstance(scene).getTree();
    this._mesh.position = this._mesh.position.add(position);

    this._slots = <TransformNode[]>this._mesh.getChildren().filter(ch=>ch.name.includes("slot"));
    this._slots.forEach(s=>{
      if(position.z<0){
      s.position = position.subtract(s.position);
      }else{
      s.position = s.position.add(position);
      }
    })
    this.shuffleArray(this._slots);

    this._decorations = <Mesh[]>this._mesh.getChildren().filter(ch=>ch.name.includes("bombka"));
    this._decorations.forEach(d=>d.isVisible = false);
    this.shuffleArray(this._decorations);
  }

  getSlot():Vector3{
    this._slotsIterator;
    if(this._slotsIterator>=this._slots.length){
      this._slotsIterator=0;
    }
    // const i = Math.floor(Math.random()*this._slots.length);
    return this._slots[this._slotsIterator++].position
  }

  public addDevoration(m:Material){
    if(this._devorationIterator<this._decorations.length){
      const decor = this._decorations[this._devorationIterator++];
      decor.isVisible = true;
      decor.material = m;
    }
  }

  private shuffleArray(array) {
      for (let i = array.length - 1; i > 0; i--) {
          const j = Math.floor(Math.random() * (i + 1));
          [array[i], array[j]] = [array[j], array[i]];
      }
  }
}
