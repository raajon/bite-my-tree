import {
  Mesh,
  Scene,
  Vector3
} from "@babylonjs/core";
import Models from "../Models";
import Tree from "./Tree";
import Player from "../game/Player";

export default class Giraffe {
  private readonly SPEED:number = 13;
  private readonly MAX_TIME_TO_CHANGE_MIND:number = 5;

  private _scene:Scene;
  private _mesh: Mesh;
  private _players:Player[];
  private _scoringPlayer:Player;
  private _trees:Tree[];
  private _angle:number = Math.PI;
  private _onTarget:boolean = false;
  private _target:Vector3 = new Vector3(0,0,0);
  private _timeToChangeMind:number = 0;
  private _giraffeWant:number=0;

  constructor(players:Player[], trees:Tree[], run:boolean, scene: Scene) {
    this._scene = scene;
    this._players = players;
    this._trees = trees;
    this._mesh = Models.getInstance(scene).getGiraffe();
    this._mesh.position = new Vector3(-30,0,0);
    this._mesh.rotation = new Vector3(50, this._angle-Math.PI/2, 0);
    if(run){
      this.run();
    }
  }

  public run(){
    this._scene.registerBeforeRender(() => {
        this.update();
    })
    setInterval(()=>{
      console.log()
      if(this._timeToChangeMind < 0){
        this._timeToChangeMind = 5 + Math.floor(Math.random()*this.MAX_TIME_TO_CHANGE_MIND);
        this.changeMind();
      }
      if(this._onTarget && this._scoringPlayer){
        this._scoringPlayer.score();
      }
      this._timeToChangeMind--;
    },1000)
  }

  private update(){
    this.move();
  }

  private move(){
    if(this._target){
      const dX = this._target.x - this._mesh.position.x;
      const dZ = this._target.z - this._mesh.position.z;

      if(Math.abs(dX)+Math.abs(dZ) > .2){
        let angle = Math.atan2(dX, dZ);

        if(Math.abs(this._angle - angle)>Math.PI){this._angle=angle}
        if(this._angle<angle){
          this._angle += 0.05
        }else{
          this._angle -= 0.05
        }

        const x = Math.sin(this._angle);
        const z = Math.cos(this._angle);

        const deltaTime = this._scene.getEngine().getDeltaTime() / 1000.0;
        this._mesh.moveWithCollisions(new Vector3(x, 0, z).scale(deltaTime * this.SPEED));
        this._mesh.rotation = new Vector3(0, this._angle-Math.PI/2, 0);
      }else {
        this._mesh.position = this._target.clone();
        this._target = null;
        this._onTarget = true;
      }
    }
  }

  private changeMind(){
    this._giraffeWant = Math.floor(Math.random()*4);
    this._onTarget = false;
    if(this._giraffeWant === 0){
      this._target = Vector3.Zero();
      this._scoringPlayer = null;
    }else{
      let p0 = 0;
      let p1 = 0;
      if(this._giraffeWant === 1){
        p0 = this._players[0].getPrice1();
        p1 = this._players[1].getPrice1();
      }else if(this._giraffeWant === 2){
        p0 = this._players[0].getPrice2();
        p1 = this._players[1].getPrice2();
      }else if(this._giraffeWant === 3){
        p0 = this._players[0].getPrice3();
        p1 = this._players[1].getPrice3();
      }
      if(p0>p1){
        this._scoringPlayer = this._players[0];
      }else if(p0<p1){
        this._scoringPlayer = this._players[1];
      }else{
        const i = Math.floor(Math.random()*2);
        this._scoringPlayer = this._players[i];
      }
      this._target = this._scoringPlayer.getSlot();
    }
    // console.log("setTarget", this._target)
  }

}
