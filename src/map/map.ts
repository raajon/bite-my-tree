import {
  DynamicTexture,
  Mesh,
  MeshBuilder,
  Scene,
  SceneLoader,
  ShadowGenerator,
  StandardMaterial,
  Texture,
  TransformNode,
  Vector3
} from "@babylonjs/core";
import Materials from "../Materials";
import Models from "../Models";


export default class Map {
    private _scene: Scene;

    private

    constructor(scene: Scene) {
        this._scene = scene;
        const ground = Models.getInstance(scene).getMap();
        for(let i=0; i<10; i++){
          const stone = Models.getInstance(scene).getStone();
          const x = Math.floor(50 - Math.random()*100);
          const z = Math.floor(50 - Math.random()*100);
          const r = Math.random()*Math.PI*2;
          const s = 0.3 + Math.random()*.8;
          stone.position = new Vector3(x,0,z);
          stone.scaling = new Vector3(s,s,s);
          stone.rotation = new Vector3(0,r,0);
        }
        for(let i=0; i<10; i++){
          const stone = Models.getInstance(scene).getBush();
          const x = Math.floor(50 - Math.random()*100)
          const z = Math.floor(50 - Math.random()*100)
          const r = Math.random()*Math.PI*2;
          const s = 0.3 + Math.random()*.8;
          stone.position = new Vector3(x,0,z);
          stone.scaling = new Vector3(s,s,s);
          stone.rotation = new Vector3(0,r,0);
        }
        // const ground = Mesh.CreatePlane("ground", 200, scene);
        ground.material = Materials.getGrassMaterial(scene);
        ground.rotation.x = Math.PI/2;
        ground.isPickable = true;

        this.load();
    }

    public async load(){

      const startTime = new Date().getTime();
      //
      // const tree1 = new Tree(new Vector3(0,0,35), this._scene);
      // const tree2 = new Tree(new Vector3(0,0,-35), this._scene);

      const duration = new Date().getTime() - startTime;
      console.log("Map generated in " + duration + "ms");
    }

}
