import { Scene, Sound } from "@babylonjs/core";

export default class GameSounds {

  private static _instance:GameSounds;
  private _dooreOpen:Sound;
  private _dooreClose:Sound;

  constructor(scene:Scene){
    GameSounds._instance = this;
    this._dooreOpen = new Sound( "Doore", "/sounds/doore.mp3", scene, null, { loop: false, autoplay: false, length: 2, offset: 0 } );
    this._dooreClose = new Sound( "Doore", "/sounds/doore.mp3", scene, null, { loop: false, autoplay: false, length: 8.5, offset: 6.5 } );
  }

  public get dooreOpen():Sound{
    return this._dooreOpen;
  }

  public get dooreClose():Sound{
    return this._dooreClose;
  }

  public static getInstance():GameSounds{
    return this._instance;
  }
}
