import { Scene } from "@babylonjs/core";
import { AdvancedDynamicTexture, Control, Ellipse, StackPanel, Rectangle, Grid, Button, TextBlock } from "@babylonjs/gui";
import PlayerUI from "./PlayerUI";
import Game from "../game/Game";

export default class Hud {

    private readonly STORY:string = "The american scientist found that Giraffes prefer to eat from the trees that are more attractive to them. Please deepen this research. Decorate your tree and  be the first to score 100 points!!!";

    private static instance: Hud;
    private _scene: Scene;

    //UI Elements
    private _ui:AdvancedDynamicTexture;
    // private _pointer:Ellipse;
    private _playersUi:PlayerUI;
    private _rectangle:Rectangle;
    private _uiBtn:Button[];

    private style = {rectWidth:'1', textWidth:'1', menuWidth:'250px', textHight:100, leftMmargin: 5, topMmargin:40, padding: 6, cornerRadius:0};
    private _startCount: number = 0;

    constructor(scene: Scene) {
        Hud.instance = this;
        this._scene = scene;

        this._ui = AdvancedDynamicTexture.CreateFullscreenUI("UI");
        this._ui.idealHeight = 720;

        this._uiBtn = []

        // this._topBar = new TopBar(this._ui);
        this._playersUi = new PlayerUI(this._ui);
        this.createPanel();
    }

    private createPanel(){
      this._rectangle = new Rectangle();
      this._rectangle.thickness = 0
      // this._rectangle.height = this.style.textHight + 'px';
      this._rectangle.width = this.style.rectWidth;
      this._rectangle.paddingLeft = '0px';
      this._rectangle.paddingBottom = '0px';
      this._rectangle.verticalAlignment = StackPanel.VERTICAL_ALIGNMENT_CENTER;
      this._rectangle.horizontalAlignment = StackPanel.HORIZONTAL_ALIGNMENT_CENTER;
      this._rectangle.background = 'black';
      this._rectangle.alpha = 0.5;
      this._rectangle.cornerRadius = this.style.cornerRadius;
      this._rectangle.isVisible = true;
      this._rectangle.rotation = 0;
      this._ui.addControl(this._rectangle);

      this._rectangle.addControl(this.createBtnPanel());
      return this._rectangle;
    }

    private createBtnPanel(){
      var grid = new StackPanel();
      this._uiBtn.push(this.createBtn(0));
      this._uiBtn.push(this.createScoreLabel(this.STORY, 0));
      this._uiBtn.push(this.createBtn(0));
      grid.addControl(this._uiBtn[0] );
      grid.addControl(this._uiBtn[1] );
      grid.addControl(this._uiBtn[2] );

      return grid;
    }

    private createScoreLabel(text, rotation){
      const btn = Button.CreateImageWithCenterTextButton("dupa", "",  "./sprites/instruction.png");
      btn.color = "white";
      btn.fontSize = 24;
      btn.height = '300px';
      btn.width = '400px';
      btn.alpha =  .8;
      btn.thickness = 0;
      btn.horizontalAlignment = Control.HORIZONTAL_ALIGNMENT_CENTER;
      btn.onPointerUpObservable.add(()=>{});
      return btn;
    }

    private createBtn(rotation){
      const btn = Button.CreateSimpleButton("btn", "Start Game!");
      btn.width = "200px"
      btn.height = "30px";
      btn.color = "white";
      btn.fontSize = 24;
      btn.cornerRadius = 20;
      btn.background = "green";
      btn.alpha = .8;
      btn.rotation = rotation
      btn.onPointerUpObservable.add(()=>this.startGame(btn));
      return btn;
    }

    private startGame(btn:Button){
      this._startCount++;
      btn.isEnabled = false;
      btn.alpha = .4;
      if(this._startCount>=2){
        Game.getInstance(null).startGame();
        this._rectangle.isVisible = false;
        // this._rectangle.height = '0px';
        this._uiBtn.forEach(b=>{
          // b.isVisible = true;
          b.isEnabled = true;
        })
      }
    }

    public update(){
      this._playersUi.update();
    }


    public static getInstance():Hud {
        return Hud.instance;
    }



}
