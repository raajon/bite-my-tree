import {
  AdvancedDynamicTexture,
  Button,
  Control,
  Grid,
  Rectangle,
  StackPanel,
  TextBlock
} from "@babylonjs/gui";
import Game from '../game/Game';
import Player from "../game/Player";

export default class PlayerUI{

  private _game:Game = Game.getInstance(null);
  private _ui:AdvancedDynamicTexture;
  private _rectangle:Rectangle;
  private _menuContainer:Rectangle;
  private _menuBtn:Button;

  private config = [
    {
      playerId: 0,
      playaerName:"Blue",
      position:StackPanel.VERTICAL_ALIGNMENT_TOP,
      bgColor:"blue",
      scoreLabel:null,
      btns:[
        {name:"btn1", img:"./sprites/decorBtn1.png", fun:null, canUpgrade:null, btn:null, price:null},
        {name:"btn2", img:"./sprites/decorBtn2.png", fun:null, canUpgrade:null, btn:null, price:null},
        {name:"btn3", img:"./sprites/decorBtn3.png", fun:null, canUpgrade:null, btn:null, price:null}
      ]
    },
    {
      playerId: 1,
      playaerName:"Red",
      position:StackPanel.VERTICAL_ALIGNMENT_BOTTOM,
      bgColor:"red",
      scoreLabel:null,
      scoreFun:null,
      btns:[
        {name:"btn1", img:"./sprites/decorBtn1.png", fun:null, canUpgrade:null, btn:null, price:null},
        {name:"btn2", img:"./sprites/decorBtn2.png", fun:null, canUpgrade:null, btn:null, price:null},
        {name:"btn3", img:"./sprites/decorBtn3.png", fun:null, canUpgrade:null, btn:null, price:null}
      ]
    }
  ]

  private style = {rectWidth:'1', textWidth:'1', menuWidth:'250px', textHight:100, leftMmargin: 5, topMmargin:40, padding: 6, cornerRadius:0};

  constructor(ui: AdvancedDynamicTexture) {
    this._ui = ui;
    this.createPlayerPanel(this.config[0], this._game.getPlayers()[0])
    this.createPlayerPanel(this.config[1], this._game.getPlayers()[1])
    console.log(this.config)
  }

  private createPlayerPanel(config:any, player:Player){
    this._rectangle = new Rectangle();
    this._rectangle.thickness = 0
    this._rectangle.height = this.style.textHight + 'px';
    this._rectangle.width = this.style.rectWidth;
    this._rectangle.paddingLeft = '0px';
    this._rectangle.paddingBottom = '0px';
    this._rectangle.verticalAlignment = config.position;
    this._rectangle.horizontalAlignment = StackPanel.HORIZONTAL_ALIGNMENT_RIGHT;
    this._rectangle.background = config.bgColor;
    this._rectangle.alpha = 0.5;
    this._rectangle.cornerRadius = this.style.cornerRadius;
    this._rectangle.isVisible = true;
    this._rectangle.rotation = config.position === StackPanel.VERTICAL_ALIGNMENT_BOTTOM ? 0 : Math.PI;
    this._ui.addControl(this._rectangle);

    this._rectangle.addControl(this.createBtnPanel(config, player));
    return this._rectangle;
  }

  private createBtnPanel(config, player:Player){
    var grid = new Grid();
    grid.addColumnDefinition(.333333);
    grid.addColumnDefinition(.333333);
    grid.addColumnDefinition(.333333);
    grid.addRowDefinition(.4);
    grid.addRowDefinition(1);

    grid.addControl(this.createScoreLabel(config, player),0,1);

    config.btns.forEach((b,i)=>{
      grid.addControl(this.createBtn(b, player.getProps().propUpgrade, player.getProps().canUpgrade, player.getProps().price, i), 1, i);
    })

    return grid;
  }

  private createScoreLabel(config, player){
    const text = new TextBlock();
    text.text = "";
    text.color = "white";
    text.fontSize = 24;
    config.scoreLabel = text;
    config.scoreFun = player.getProps().score
    return text;
  }

  private createBtn(b, upgradeFun, canUpgrade, price, i){
    const btn = Button.CreateImageWithCenterTextButton(b.name, "",  b.img);
    btn.color = "white";
    btn.fontSize = 24;
    btn.height = this.style.textHight*2/3 + 'px';
    btn.width = this.style.textHight + 'px';
    btn.alpha = canUpgrade[i] ? .8 : .4;
    btn.thickness = 0;
    btn.horizontalAlignment = Control.HORIZONTAL_ALIGNMENT_CENTER;
    btn.onPointerUpObservable.add(upgradeFun[i]);
    b.btn = btn;
    b.fun = upgradeFun[i];
    b.canUpgrade = canUpgrade[i];
    b.price = price[i];
    return btn;
  }

  public update(){
    this.config.forEach((p,i)=>{
      p.scoreLabel.text = "score: " + p.scoreFun();
      p.btns.forEach((b,j)=>{
        b.btn.alpha = b.canUpgrade() ? .8 : .4;
        b.btn.textBlock.text = String(b.price());
      })
    });
  }

}
