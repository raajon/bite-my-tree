import { Color4, DefaultLoadingScreen, Effect, Engine, EngineFactory, FreeCamera, PostProcess, Scene, WebGPUEngine, Vector3 } from "@babylonjs/core";
import "@babylonjs/core/Debug/debugLayer";
import { AdvancedDynamicTexture, Button, Control, Image, InputText, Rectangle, TextBlock } from "@babylonjs/gui";
import "@babylonjs/inspector";
import "@babylonjs/loaders/glTF";
import * as CANNON from "cannon";
import * as earcut from "earcut";
import { InputController } from "./inputController";
import { Environment } from "./map/environment";
import Game from "./game/Game";
import Hud from "./ui/Hud";
import ScreenInfo from './utils/screenInfo';
import Models from "./Models";

(window as any).earcut = earcut;

//enum for states
enum State { START = 0, GAME = 1, LOSE = 2, CUTSCENE = 3 }

// App class is our entire game application
class App {
    // General Entire Application
    private _scene: Scene;
    private _canvas: HTMLCanvasElement;
    private _engine: Engine;

    private _screenInfo = ScreenInfo.getInstance();

    //Game State Related
    public assets;
    private _input: InputController;
    private _game: Game;
    private _hud: Hud;
    private _environment;

    //Scene - related
    private _state: number = 0;
    private _gamescene: Scene;
    private _cutScene: Scene;

    //post process
    private _transition: boolean = false;

    private _defaultLogoOverwrite () {
        DefaultLoadingScreen.DefaultLogoUrl = "/sprites/cicd_logo.png"
        //DefaultLoadingScreen.DefaultSpinnerUrl = "URL"
    }

    constructor() {
        this._canvas = this._createCanvas();
        this._defaultLogoOverwrite();

        // initialize babylon scene and engine
        this._init();
    }

    private async _init(): Promise<void> {
        this._engine = (await EngineFactory.CreateAsync(this._canvas, undefined)) as Engine;
        // this._engine = new WebGPUEngine(this._canvas);
        // await (<WebGPUEngine>this._engine).initAsync();
        this._engine.setHardwareScalingLevel(1.5);

        this._scene = new Scene(this._engine);

        this._scene.collisionsEnabled = false;


        //**for development: make inspector visible/invisible
        window.addEventListener("keydown", (ev) => {
            //Shift+Ctrl+Alt+I
            if (ev.shiftKey && ev.ctrlKey && ev.altKey && ev.keyCode === 73) {
                if (this._scene.debugLayer.isVisible()) {
                    this._scene.debugLayer.hide();
                } else {
                    this._scene.debugLayer.show();
                }
            }
        });

        //MAIN render loop & state machine
        await this._main();
    }

    private async _main(): Promise<void> {
        await this._goToStart();

        // Register a render loop to repeatedly render the scene
        this._engine.runRenderLoop(() => {
            switch (this._state) {
                case State.START:
                    this._scene.render();
                    break;
                case State.CUTSCENE:
                    this._scene.render();
                    break;
                case State.GAME:
                    this._scene.render();
                    break;
                case State.LOSE:
                    this._scene.render();
                    break;
                default: break;
            }
        });

        //resize if the screen is resized/rotated
        window.addEventListener('resize', () => {
            this._engine.resize();
        });
    }

    //set up the canvas
    private _createCanvas(): HTMLCanvasElement {
        var elem = document.getElementById("wellcomeLoader");
        elem.parentNode.removeChild(elem);

        //Commented out for development
        document.documentElement.style["overflow"] = "hidden";
        document.documentElement.style.overflow = "hidden";
        document.documentElement.style.width = "100%";
        document.documentElement.style.height = "100%";
        document.documentElement.style.margin = "0";
        document.documentElement.style.padding = "0";
        document.body.style.overflow = "hidden";
        document.body.style.width = "100%";
        document.body.style.height = "100%";
        document.body.style.margin = "0";
        document.body.style.padding = "0";
        document.body.style.background = "#202528";

        //create the canvas html element and attach it to the webpage
        this._canvas = document.createElement("canvas");
        this._canvas.style.width = "100%";
        this._canvas.style.height = "100%";
        this._canvas.id = "gameCanvas";
        document.body.appendChild(this._canvas);

        return this._canvas;
    }

    // goToStart
    private async _goToStart() {
        this._engine.displayLoadingUI(); //make sure to wait for start to load

        //--SCENE SETUP--
        //dont detect any inputs from this ui while the game is loading
        this._scene.detachControl();
        let scene = new Scene(this._engine);
        scene.clearColor = new Color4(.125, .145, .157, 1);
        //creates and positions a free camera
        let camera = new FreeCamera("camera1", new Vector3(0, 0, 0), scene);
        camera.setTarget(Vector3.Zero()); //targets the camera to scene origin

        //--GUI--
        const guiMenu = AdvancedDynamicTexture.CreateFullscreenUI("UI");
        guiMenu.idealHeight = 720;

        let logoSize = 0;
        if(this._screenInfo.isMobile()){
          if(this._screenInfo.isPortrait()){
            logoSize = this._screenInfo.getWidth();
          }else{
            logoSize = this._screenInfo.getWidth()*0.7;
          }
        }else{
          logoSize = this._screenInfo.getHeight()*0.7;
        }

        //background image
        const imageRect = new Rectangle("titleContainer");
        imageRect.verticalAlignment = Control.VERTICAL_ALIGNMENT_TOP;
        imageRect.horizontalAlignment = Control.HORIZONTAL_ALIGNMENT_CENTER;
        imageRect.top = '20px'
        imageRect.width = logoSize+'px';
        imageRect.height = logoSize+'px';
        imageRect.thickness = 0;
        guiMenu.addControl(imageRect);

        const startbg = new Image("startbg", "sprites/logo.png");
        imageRect.addControl(startbg);


        const startBtn = Button.CreateSimpleButton("start", "PLAY");
        startBtn.fontFamily = "Viga";
        startBtn.width = 0.2
        startBtn.height = "40px";
        startBtn.color = "white";
        startBtn.background = "black";
        startBtn.top = "-64px";
        startBtn.thickness = 1;
        startBtn.cornerRadius = 5;
        startBtn.horizontalAlignment = Control.HORIZONTAL_ALIGNMENT_CENTER;
        startBtn.verticalAlignment = Control.VERTICAL_ALIGNMENT_BOTTOM;
        guiMenu.addControl(startBtn);

        const msgBlock = new TextBlock();
        // msgBlock.fontSize = config.fontSize;
        msgBlock.color =  "red";
        msgBlock.text = "";
        msgBlock.resizeToFit = true;
        msgBlock.verticalAlignment = 0;
        msgBlock.horizontalAlignment = 0;
        msgBlock.height =  "40px";
        msgBlock.width = "300px";
        msgBlock.fontFamily = "Viga";
        msgBlock.top = "-144px";
        msgBlock.horizontalAlignment = Control.HORIZONTAL_ALIGNMENT_CENTER;
        msgBlock.verticalAlignment = Control.VERTICAL_ALIGNMENT_BOTTOM;
        guiMenu.addControl(msgBlock);

        //set up transition effect : modified version of https://www.babylonjs-playground.com/#2FGYE8#0
        Effect.RegisterShader("fade",
            "precision highp float;" +
            "varying vec2 vUV;" +
            "uniform sampler2D textureSampler; " +
            "uniform float fadeLevel; " +
            "void main(void){" +
            "vec4 baseColor = texture2D(textureSampler, vUV) * fadeLevel;" +
            "baseColor.a = 1.0;" +
            "gl_FragColor = baseColor;" +
            "}");

        let fadeLevel = 1.0;
        this._transition = false;
        scene.registerBeforeRender(() => {
            if (this._transition) {
                fadeLevel -= .05;
                if(fadeLevel <= 0){
                    this._goToCutScene();
                    this._transition = false;
                }
            }
        })

        //this handles interactions with the start button attached to the scene
        startBtn.onPointerUpObservable.add(() => {
            scene.detachControl(); //observables disabled
            msgBlock.color =  "white";
            msgBlock.text = "Loading...";
            startBtn.isVisible = false;
            //   //fade screen
            const postProcess = new PostProcess("Fade", "fade", ["fadeLevel"], null, 1.0, camera);
            postProcess.onApply = (effect) => {
                effect.setFloat("fadeLevel", fadeLevel);
            };
            this._transition = true;
        });

        //--SCENE FINISHED LOADING--
        await scene.whenReadyAsync();
        this._engine.hideLoadingUI(); //when the scene is ready, hide loading
        //lastly set the current state to the start state and set the scene to the start scene
        this._scene.dispose();
        this._scene = scene;
        this._state = State.START;
    }

    private async _goToCutScene(): Promise<void> {
        this._engine.displayLoadingUI();
        //--SETUP SCENE--
        //dont detect any inputs from this ui while the game is loading
        this._scene.detachControl();
        this._cutScene = new Scene(this._engine);
        let camera = new FreeCamera("camera1", new Vector3(0, 0, 0), this._cutScene);
        camera.setTarget(Vector3.Zero());
        this._cutScene.clearColor = new Color4(0, 0, 0, 1);

        //--GUI--
        const cutScene = AdvancedDynamicTexture.CreateFullscreenUI("cutscene");
        let canplay = true;

        // //sets up the state machines for animations
        this._cutScene.onBeforeRenderObservable.add(() => {
            //only once all of the game assets have finished loading and you've completed the animation sequence + dialogue can you go to the game state
            if(finishedLoading && canplay) {
                canplay = false;
                this._goToGame();
            }
        })

        //--WHEN SCENE IS FINISHED LOADING--
        await this._cutScene.whenReadyAsync();
        this._scene.dispose();
        this._state = State.CUTSCENE;
        this._scene = this._cutScene;

        //--START LOADING AND SETTING UP THE GAME DURING THIS SCENE--
        var finishedLoading = false;
        await this._setUpGame().then(res =>{
            finishedLoading = true;

        });
    }

    private async _setUpGame() { //async
        //--CREATE SCENE--
        let scene = new Scene(this._engine);
        const models = Models.getInstance(scene);
        await models.load();

        this._gamescene = scene;
        //dont detect any inputs from this ui while the game is loading
        scene.detachControl();

        //--INPUT--
        this._input = new InputController(scene, this._hud); //detect keyboard/mobile inputs
        // this._game.initSounds(scene);
        this._game = Game.getInstance(scene);
        this._game.createPlayer(this.assets, scene, null, this._canvas, this._input);
        //--GUI--
        this._hud = new Hud(scene);

        //--CREATE ENVIRONMENT--
        const environment = new Environment(scene);
        this._environment = environment;
        //Load environment and character assets

        await this._environment.load(); //environment
    }

    //goToGame
    private async _goToGame(): Promise<void> {
        //--SETUP SCENE--
        this._scene.detachControl();
        let scene = this._gamescene;

        //Initializes the game's loop
        await this._initializeGameAsync(scene); //handles scene related updates & setting up meshes in scene

        //--WHEN SCENE FINISHED LOADING--
        await scene.whenReadyAsync();

        //get rid of start scene, switch to gamescene and change states
        this._scene.dispose();
        this._state = State.GAME;
        this._scene = scene;
        this._engine.hideLoadingUI();
        //the game is ready, attach control back
        this._scene.attachControl();

    }

    //init game
    private async _initializeGameAsync(scene): Promise<void> {
        // this._game.startGame();
        // setInterval(() => game.sendClientState());

        //--GAME LOOP--
        scene.onBeforeRenderObservable.add(() => {
            // this._game.update();
            // this._hud.updateHud(this._game);
        })
    }

}
new App();
