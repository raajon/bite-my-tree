import {
  Color3,
  CubeTexture,
  TouchCamera,
  FreeCamera,
  FreeCameraMouseInput,
  FreeCameraTouchInput,
  HighlightLayer,
  Mesh,
  MeshBuilder,
  Ray,
  RayHelper,
  Scene,
  ShadowGenerator,
  Sound,
  StandardMaterial,
  Texture,
  UniversalCamera,
  Vector3
} from "@babylonjs/core";
import { InputController } from "./inputController";
import Game from "./game/Game";

export default class Camera {

  // //const values
  private static readonly VISIBLE_BORDER :string = "visibleBorder";

  private _visibleBorder:Mesh;
  private _lockCamerPosition = false;

  private _assets;
  private _camera: FreeCamera;
  private _input: InputController;
  private _scene: Scene;
  private _shadowGenerator: ShadowGenerator;
  private _highlightLayer: HighlightLayer;
  private _game: Game = Game.getInstance(null);
  private _stepSound:Sound;

  constructor(assets, scene: Scene, shadowGenerator: ShadowGenerator, canvas: HTMLCanvasElement, input?: InputController) {

    this._stepSound = new Sound("sounds", "/sounds/steps.mp3", scene, null, { loop: true, autoplay: false });

    this._assets = assets;
    this._input = input;
    this._scene = scene;
    this._shadowGenerator = shadowGenerator;
    this._highlightLayer = new HighlightLayer("hl1",   this._scene);

    this._visibleBorder = MeshBuilder.CreateBox(Camera.VISIBLE_BORDER, {size:5}, scene);
    this._visibleBorder.position.z = 50;

    this.initCamera(canvas);

    this._scene.registerBeforeRender(() => {
        this.positionCamera();
    })
  }

  private initCamera(canvas: HTMLCanvasElement) {
    this._camera = new FreeCamera("UniversalCamera", new Vector3(0, 1, 0), this._scene);
    this._camera.setTarget(new Vector3(0,0,0));
    this._camera.applyGravity = false;
    // this._camera.ellipsoid = new Vector3(1, Player.PLAYER_HEIGHT/2, 1);
    // this._camera.checkCollisions = true;
    this._camera.attachControl(canvas, true);
    this._camera.inputs.removeByType("FreeCameraKeyboardMoveInput");
    this._camera.inputs.removeByType("FreeCameraMouseInput");
    this._camera.speed = 0.5;
    this._camera.minZ = 0.1;
    this._camera.maxZ = 250;
    this._camera.angularSensibility = 4000;

     this._camera._initialFocalDistance = 20;
    // console.log("projectionPlaneTilt", this._camera.projectionPlaneTilt);
  }

  private positionCamera(){
    if(!this._lockCamerPosition){
      const origin = this._camera.position;
      const forward = this.vecToLocal(new Vector3(0,-1,2.4), this._camera);
      const direction = Vector3.Normalize(forward.subtract(origin));
      const length = 1000;

      const  ray = new Ray(this._camera.position, direction, length);

      const hit = this._scene.pickWithRay(ray);
      if (hit.pickedMesh && hit.pickedMesh.name === Camera.VISIBLE_BORDER){
        this._lockCamerPosition = true;
        this._visibleBorder.visibility = 0;
        console.log("Camera height:", this._camera.position.y);
        // this._game.startGame();
      }

      this._camera.position.y += .5;
    }
  }

  private vecToLocal(vector, mesh){
    const m = mesh.getWorldMatrix();
    const v = Vector3.TransformCoordinates(vector, m);
    return v;
  }

}
