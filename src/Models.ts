import { Scene, MeshBuilder, Mesh, SceneLoader, AbstractMesh } from "@babylonjs/core";


export default class Models{

  private _scene: Scene;
  private static _instance: Models;
  private _map:Mesh;
  private _stone:Mesh;
  private _bush:Mesh;
  private _tree:Mesh;
  private _giraffe:Mesh

  constructor(scene:Scene){
    this._scene = scene;
  }

  public async load(){
    this._map = <Mesh>(await SceneLoader.ImportMeshAsync("", "/models/", "map.glb", this._scene)).meshes[0];
    this._map.position.z = -1000;

    this._bush = <Mesh>(await SceneLoader.ImportMeshAsync("", "/models/", "bush.glb", this._scene)).meshes[0];
    this._bush.position.z = -1000;

    this._stone = <Mesh>(await SceneLoader.ImportMeshAsync("", "/models/", "stone.glb", this._scene)).meshes[0];
    this._stone.position.z = -1000;

    this._tree = <Mesh>(await SceneLoader.ImportMeshAsync("", "/models/", "tree.glb", this._scene)).meshes[0];
    this._tree.position.z = -1000;

    this._giraffe = <Mesh>(await SceneLoader.ImportMeshAsync("", "/models/", "giraffe2.glb", this._scene)).meshes[0];
    this._giraffe.position.z = -1000;
  }

  public getMap(){
      const map = this._map.clone();
      map.position.z = 0;
      return map;
  }

  public getBush(){
      const bush = this._bush.clone();
      bush.position.z = 0;
      return bush;
  }

  public getStone(){
      const stone = this._stone.clone();
      stone.position.z = 0;
      return stone;
  }

  public getTree(){
    const tree = this._tree.clone();
    tree.position.z = 0;
    return tree;
  }

  public getGiraffe(){
    const giraffe = this._giraffe.clone();
    giraffe.position.z = 0;
    return giraffe;
  }

  public static getInstance(scene){
    if(!Models._instance){
      Models._instance = new Models(scene);
    }
    return Models._instance;
  }
}
