import Hud from "../ui/Hud";
import Tree from "../map/Tree";
import { TransformNode, Vector3, Scene } from "@babylonjs/core";
import Materials from "../Materials";


export default class Player{

  private readonly SCORE = 0.5;

  private _scene:Scene;
  private _tree:Tree;

  private _score:number = 10;
  private _prop1:number = 0;
  private _prop2:number = 0;
  private _prop3:number = 0;
  private _treeAttractivness:number = 0;


  constructor(tree:Tree, scene:Scene){
    this._scene = scene;
    this._tree = tree;
  }

  public getProps() {
    return{
      score: this.getScore,
      propUpgrade: [this.upgradeProp1, this.upgradeProp2, this.upgradeProp3],
      canUpgrade: [this.canUpgradeProp1, this.canUpgradeProp2, this.canUpgradeProp3],
      price: [this.getPrice1, this.getPrice2, this.getPrice3]
    }
  }

  public getTreeAttractivness(){
    return this._treeAttractivness;
  }

  public upgradeProp1 = () =>{
    if(this.canUpgradeProp1()){
      this._treeAttractivness += this._prop1;
      this._score -= (this._prop1+1);
      this._prop1++;
      this._tree.addDevoration(Materials.getRedBombMaterial(this._scene))
    }
  }

  public upgradeProp2 = () =>{
    if(this.canUpgradeProp2()){
      this._treeAttractivness += this._prop2;
      this._score -= (this._prop2+1);
      this._prop2++;
      this._tree.addDevoration(Materials.getBlueBombMaterial(this._scene))
    }
  }

  public upgradeProp3 = () =>{
    if(this.canUpgradeProp3()){
      this._treeAttractivness += this._prop3;
      this._score -= (this._prop3+1);
      this._prop3++;
      this._tree.addDevoration(Materials.getPinkBombMaterial(this._scene))
    }
  }

  public canUpgradeProp1 = ():boolean =>{
    return this._score>=(this._prop1+1)
  }

  public canUpgradeProp2 = ():boolean =>{
    return this._score>=(this._prop2+1)
  }

  public canUpgradeProp3 = ():boolean =>{
    return this._score>=(this._prop3+1)
  }

  public getPrice1 = ():number =>{
    return this._prop1;
  }

  public getPrice2 = ():number =>{
    return this._prop2;
  }

  public getPrice3 = ():number =>{
    return this._prop3;
  }

  public getScore = ():number =>{
    return this._score;
  }

  public score(){
    this._score += this.SCORE;
  }

  public getSlot():Vector3{
    return this._tree.getSlot();
  }

}
