import {
  Scene,
  ShadowGenerator,
  Vector3
} from "@babylonjs/core";
import { InputController } from "../inputController";
import Camera from '../Camera';
import GameSounds from '../GameSounds';
import Player from "./Player";
import Hud from "../ui/Hud";
import Tree from "../map/Tree";
import Giraffe from "../map/Giraffe";

export default class Game {

  private static _instance: Game;
  private _scene:Scene;
  private _camera:Camera;
  private _players:Player[] = []
  private _giraffes:Giraffe[] = [];

  constructor(scene:Scene){
      const tree0 = new Tree(new Vector3(0,0,-27), scene);
      const tree1 = new Tree(new Vector3(0,0,27), scene);
      this._players.push(new Player(tree0, scene))
      this._players.push(new Player(tree1, scene))

      this._giraffes.push(new Giraffe(this._players, [tree0, tree1], false, scene));
      setTimeout(()=> this._giraffes.push(new Giraffe(this._players, [tree0, tree1], true, scene)), 10000);
      setTimeout(()=> this._giraffes.push(new Giraffe(this._players, [tree0, tree1], true, scene)), 20000);
      setTimeout(()=> this._giraffes.push(new Giraffe(this._players, [tree0, tree1], true, scene)), 30000);
      setTimeout(()=> this._giraffes.push(new Giraffe(this._players, [tree0, tree1], true, scene)), 40000);
      setTimeout(()=> this._giraffes.push(new Giraffe(this._players, [tree0, tree1], true, scene)), 50000);

      scene.registerBeforeRender(() => {
          this.update();
      })
  }

  private update(){
  }

  public startGame(){
    console.log("start game")
    this._giraffes.forEach(g=>g.run());
    setInterval(()=>{
      Hud.getInstance().update();
    },100)
  }

  public createPlayer(assets, scene: Scene, shadowGenerator: ShadowGenerator, canvas: HTMLCanvasElement, input?: InputController) {
      this._camera = new Camera(assets, scene, shadowGenerator, canvas, input);
      this._scene = scene;
  }


  public getPlayers():Player[]{
    return this._players
  }

  public static getInstance(scene:Scene):Game{
    if(!this._instance){
      this._instance = new Game(scene);
    }
    return this._instance;
  }

  public initSounds(scene:Scene){
      new GameSounds(scene);
  }

}
